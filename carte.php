<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" sizes="16x16" href="./images/logo.png">
    <link rel="stylesheet" href="./styleG.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>CARTE</title>
</head>

<body>
    <header>
        <div>
            <div>
                <img src="./images/logo.png" alt="logo">
            </div>
            <h1> Dead Cow Dinner </h1>
        </div>

        <nav>
            <ul>
                <li>
                    <a class="Nav_Page_i" href="./carte.php">Notre Carte</a>
                </li>
                <li>
                    <a class="Nav_Page_i" href="./info_rest.php">Notre Restaurant</a>
                </li>
                <li>
                    <a class="Nav_Page_i" href="./reserver.php">Réserver une table</a>
                </li>
                <li>
                    <a class="Nav_Page_i" id="Uber" href="./carte.php">Commander chez Uber Eat</a>
                </li>
            </ul>
        </nav>

    </header>

    <main>
        <h2>- Notre Carte -</h2>
        <div class="boxcarte">
            <div class="burger avant">
                <div class="imgcarte">
                    <img src="./images/b1.jpg" alt="burger1">
                </div>
                <div class="txtcarte">
                    <h3>NOM</h3>
                    <h4>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum
                        molestias similique voluptate repudiandae cupiditate error natus
                        dolore debitis accusantium soluta?
                    </h4>
                    <h3 class="prix">PRIX</h3>
                </div>
            </div>
            <hr>
            <div class="burger">
                <div class="imgcarte">
                    <img src="./images/b2.png" alt="burger2">
                </div>
                <div class="txtcarte">
                    <h3>NOM</h3>
                    <h4>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum
                        molestias similique voluptate repudiandae cupiditate error natus
                        dolore debitis accusantium soluta?
                    </h4>
                    <h3 class="prix">PRIX</h3>
                </div>
            </div>
            <div class="burger">
                <div class="imgcarte">
                    <img src="./images/b3.png" alt="burger3">
                </div>
                <div class="txtcarte">
                    <h3>NOM</h3>
                    <h4>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum
                        molestias similique voluptate repudiandae cupiditate error natus
                        dolore debitis accusantium soluta?
                    </h4>
                    <h3 class="prix">PRIX</h3>
                </div>
            </div>
            <div class="burger">
                <div class="imgcarte">
                    <img src="./images/b4.png" alt="burger4">
                </div>
                <div class="txtcarte">
                    <h3>NOM</h3>
                    <h4>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum
                        molestias similique voluptate repudiandae cupiditate error natus
                        dolore debitis accusantium soluta?
                    </h4>
                    <h3 class="prix">PRIX</h3>
                </div>
            </div>
        </div>
    </main>
</body>

</html>