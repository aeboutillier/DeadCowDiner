<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="./images/logo.png">
    <title>RESERVATION</title>

    <link rel="stylesheet" href="./styleG.css" type="text/css">

    <!-- Fonts - Police -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&family=Praise&display=swap" rel="stylesheet">


</head>

<body>

    <header>
        <div>
            <div>
                <img src="./images/logo.png" alt="logo">
            </div>
            <h1> Dead Cow Dinner </h1>
        </div>

        <nav>
            <ul>
                <li>
                    <a class="Nav_Page_i" href="./carte.php">Notre Carte</a>
                </li>
                <li>
                    <a class="Nav_Page_i" href="./info_rest.php">Notre Restaurant</a>
                </li>
                <li>
                    <a class="Nav_Page_i" href="./reserver.php">Réserver une table</a>
                </li>
                <li>
                    <a class="Nav_Page_i" href="./carte.php">Commander chez Uber Eat</a>
                </li>
            </ul>
        </nav>

    </header>

    <main>
        <!-- MAIN reserver.php --- Cyril Galera ----------------------------------------->

        <div id="Titre_2">
            <h2> - Réserver une Table - </h2>
        </div>

        <!-- <form action="./login.php" method="post"> -->
        <form id="form_reservation">

            <!-- Ligne: NOM -->
            <label for="Nom_text" id="Nom_tx">Nom:</label>
            <input type="text" id="Nom_input" name="Nom_text" minlength="2" maxlength="30" size="10" required>

            <!-- Ligne: TEL -->
            <label for="Tel_text" id="Tel_tx">Téléphone:</label>
            <input type="tel" id="Tel_input" name="Tel_text" pattern="[0-9]{8}" minlength="8" maxlength="8" required>

            <!-- Ligne: DATE -->
            <label for="Date_text" id="Date_tx">Date:</label>
            <input type="date" id="Date_input" name="Date_text" value="2021-11-25" min="2021-11-25" max="2021-12-31"
                required>

            <!-- MIDI - SOIR -->
            <div id="Midi_box" class="box_time">
                <label for="midi" id="Midi_tx">Midi</label>
                <input type="checkbox" id="midi_input" name="midi">
                <!-- checked> -->
            </div>

            <div id="Soir_box" class="box_time">
                <label for="soir" id="Soir_tx">Soir</label>
                <input type="checkbox" id="soir_input" name="soir">
                <!-- checked> -->
            </div>

            <!-- Bouton RESERVER -->
            <button class="btn" id="Btn_Reserver">Réserver</button>

        </form>
    </main>

</body>

</html>